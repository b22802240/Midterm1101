<%-- 
    Document   : index
    Created on : Nov 8, 2021, 1:10:37 PM
    Author     : lendle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String pic = "question";
            /*
            Q1.
            從 cookie 中找出 “food” 的內容，
            並將其儲存至 pic 變數中 （30%）
            */
            String f =request.getParameter("food");
            Cookie cookie =new Cookie("food",f);
            cookie.setMaxAge(500);
            response.addCookie(cookie);
            response.sendRedirect("cookie.jsp");
            response.setContentType("text/html;charset=UTF-8");
            response.setCharacterEncoding("utf-8");
            Cookie [] cookie =request.getCookie();
            String pic =request.getParameter("food");



        %>
        <img style="position: absolute; left: 50px; right: 50px; top: 100px; bottom: 200px" src="toppng.com-hands-960x569.png"></img>
        <!--
        Q2.
        
        底下 img 的 src，請用 jsp 顯示 pic 變數的內容 （20%）
        -->
        
        <img id="foodImage" style="position: absolute; left: 350px; top: 300px; width: 200px" src="test1.jsp"></img>
        <p>
        <!--
        Q3.
        將表單指定成 GET 的另外一種 (10%)
        <%
            response.setContentType("text/html;charset=UTF-8");
            response.setCharacterEncoding("utf-8");
            HttpSession session=request.getSession();
            String f =request.getParameter("food");
            Session.setAttrbute("food",f);
            
            
            
            
            
            
            
            response.sendRedirect("SetFoodServlet.java");
        %>
        
        Q4.
        將表單送出後指定到 SetFoodServlet (10%)
        
        -->
        <form>
            Choose: <select name="food">
                <option value="png-clipart-hamburger-hamburger-food">漢堡</option>
                <option value="ramen">拉麪</option>
                <option value="chicken">炸雞</option>
            </select>
            <input type="submit"/>
        </form>
    </p>
</body>
</html>
